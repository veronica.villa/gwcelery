bilby-pipe >= 0.3.8
celery[redis] == 4.4.0
comet
corner
dnspython  # silence "DNS: dnspython not found. Can not use SRV lookup." warning from SleekXMPP
flask
flask-caching
flower
gwdatafind
gwpy
healpy
imapclient
jinja2
lalsuite == 6.64
ligo-followup-advocate >= 1.1.3
ligo-gracedb >= 2.3.0
ligo-raven >= 1.17
ligo-segments
ligo.skymap >= 0.1.13
lscsoft-glue
lxml
numpy
p_astro == 0.8.1
pygcn >= 0.1.19
pytest >= 3.0
python-ligo-lw
safe-netrc
seaborn
sentry-sdk[flask]
service_identity  # We don't actually use this package, but it silences some annoying warnings from twistd.
sleek-lvalert >= 0.0.4
tornado < 6  # Tornado 6.0.0 breaks Flower because it removed tornado.web.asynchronous
voeventlib >= 1.2
werkzeug >= 0.15.0  # for werkzeug.middleware.proxy_fix.ProxyFix
